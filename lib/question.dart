import 'package:flutter/material.dart';

import './answer.dart';

class Question extends StatelessWidget {
  final String _question;
  final List<Answer> _answers;

  const Question(this._question, this._answers, {Key? key}) : super(key: key);

  Question.fromStrings(
      String question, List<String> answers, void Function() answerQuestion,
      {Key? key})
      : _question = question,
        _answers =
            answers.map((answer) => Answer(answer, answerQuestion)).toList(),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      child: _getContent(),
      width: double.infinity,
    );
  }

  Widget _getContent() => Column(
        children: [
          Container(
            child: Text(
              _question,
              textAlign: TextAlign.center,
              style: const TextStyle(fontSize: 30),
            ),
            margin: const EdgeInsets.fromLTRB(0, 20, 0, 20),
          ),
          ..._answers
        ],
      );
}
