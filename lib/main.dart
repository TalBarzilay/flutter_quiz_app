import 'package:flutter/material.dart';

import './question.dart';
import './finish.dart';

void main() => runApp(const MyApp());

// ----------------------------

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  int _qIndex = 0;
  late List<Question> _questions;

  Question _createQuestionObject(String question, List<String> answers) =>
      Question.fromStrings(question, answers, () => setState(() => _qIndex++));

  @override
  void initState() {
    super.initState();

    _questions = [
      _createQuestionObject(
        "what is you favorite color?",
        const ["red", "gray", "black", "purple", "cat", "blue"],
      ),
      _createQuestionObject(
        "what is you favorite food?",
        const ["hamburger", "pizza", "black", "brokoli", "dirt", "food"],
      ),
      _createQuestionObject(
        "what is your favorite sport?",
        const ["running", "sleeping"],
      ),
      _createQuestionObject(
        "pick a number",
        const [
          "e",
          "0",
          "-2",
          "42",
          "2.5",
          "1/3",
          "e^(i*pie)",
          "banana",
          "sqrt(2)",
          "infinity",
          "i",
          "log(-7)",
        ],
      ),
    ];
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        primarySwatch: Colors.lightBlue,
      ),
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Hello World!'),
        ),
        body: (_qIndex < _questions.length)
            ? _questions[_qIndex]
            : Finish(() => setState(() => _qIndex = 0)),
      ),
    );
  }
}
