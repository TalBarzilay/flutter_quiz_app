import 'package:flutter/material.dart';

class Answer extends StatelessWidget {
  final String _answer;
  final void Function() _answerQuestion;

  const Answer(this._answer, this._answerQuestion, {Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: ElevatedButton(
        onPressed: _answerQuestion,
        child: Text(_answer),
      ),
      width: double.infinity,
      margin: const EdgeInsets.fromLTRB(25, 3, 25, 3),
    );
  }
}
